-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2018 at 02:46 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resource_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(6, '2016_06_01_000004_create_oauth_clients_table', 2),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('03e08927d945a415f71f439ebc0b01efe88c0c537d6fc98e0541cfc468dab48e4584fdada6b9fdf8', 1, 3, NULL, '[]', 0, '2018-05-07 07:30:13', '2018-05-07 07:30:13', '2019-05-07 13:30:13'),
('051f1cfe4c9e62acfe23caabc5fb301125c6a73a604c5e43a603078fdd126d591413e13a20ea9045', 1, 3, NULL, '[]', 0, '2018-05-07 07:28:15', '2018-05-07 07:28:15', '2019-05-07 13:28:15'),
('06e81f3e4cb247bd1c84fb24d38fa984595bcfc5a2219e88511a3aec770d4ac0823ceb0e1a37c73e', 1, 3, NULL, '[]', 0, '2018-05-07 05:43:06', '2018-05-07 05:43:06', '2019-05-07 11:43:06'),
('158704499c47a5f7f4cecfe8e833b1765c989a1ed4978d8aaf7aaa287cf046f7ca690d5b3c7f6e51', 1, 3, NULL, '[]', 0, '2018-05-07 07:45:11', '2018-05-07 07:45:11', '2019-05-07 13:45:11'),
('192b241470314dd80c309cbbfe89d324e484b202edd57982b3cae454d2e1d1c1e8e27fb189ec129a', 1, 3, NULL, '[]', 0, '2018-05-07 07:38:04', '2018-05-07 07:38:04', '2019-05-07 13:38:04'),
('2248122d721be2a66344d26c5f8d0899a25a2dfac9603cca075f13ce563a99574464924e2b4cf8a2', 1, 3, NULL, '[]', 0, '2018-05-07 07:35:26', '2018-05-07 07:35:26', '2019-05-07 13:35:26'),
('23e7b375fbff128214e471237028de5c9140dd31f84c61802855d4b6c4ed908640f1bb761af7b572', 1, 3, NULL, '[]', 0, '2018-05-07 06:51:08', '2018-05-07 06:51:08', '2019-05-07 12:51:08'),
('29ba8c379ddf5adc867217d3931dfef4b75c4228e19a2caa18cb1ccdf4f774957f1cfee511a84cc8', 1, 3, NULL, '[]', 0, '2018-05-07 05:41:40', '2018-05-07 05:41:40', '2019-05-07 11:41:40'),
('305bf6053fa42bf6119b4cbd8192c07cbfb0e2361fc6b3f499de592530df0ab17139399cac454126', 1, 3, NULL, '[]', 0, '2018-05-07 07:36:37', '2018-05-07 07:36:37', '2019-05-07 13:36:37'),
('377ed02a1096879236e848c540f0c735076da7e73a0c7997b2ce962748c20f5c80989ae6f7ce6d50', 1, 3, NULL, '[]', 0, '2018-05-07 07:34:39', '2018-05-07 07:34:39', '2019-05-07 13:34:39'),
('3b1e3991303b685d5b7a0f044ac0961c5604ecdffd5741b99b157de99218fa6530eedb4368f62589', 1, 3, NULL, '[]', 0, '2018-05-07 08:08:37', '2018-05-07 08:08:37', '2019-05-07 14:08:37'),
('3c4cdbfbe496eb027cf437e6c3922f4715ed288f10816f413506d4436abf60edb253efc546498bc2', 1, 3, NULL, '[]', 0, '2018-05-07 06:51:24', '2018-05-07 06:51:24', '2019-05-07 12:51:24'),
('3c888b9ec833ef8d2ec2b327594df264ac3ec92dc3173fe9bbac3edd0fac0ae134b6718bb146390c', 1, 3, NULL, '[]', 0, '2018-05-07 07:30:47', '2018-05-07 07:30:47', '2019-05-07 13:30:47'),
('3d5a12846cab434ea96964eb2c66b1c086eb4fa119ee4619f813a91e1d584a96f816531b918a9ad3', 1, 3, NULL, '[]', 0, '2018-05-07 07:44:11', '2018-05-07 07:44:11', '2019-05-07 13:44:11'),
('4e5cfd9896cea90ac8e009b44114311fc766b95e92c816d720d619c221bcc08d6e618e9dbc0644fc', 1, 3, NULL, '[]', 0, '2018-05-07 07:38:41', '2018-05-07 07:38:41', '2019-05-07 13:38:41'),
('530f72b7d4849effdfedab38a8db8a0730f7932565c3036218949038acfcc9da5fca4d2e8c3ac9a6', 1, 3, NULL, '[]', 0, '2018-05-07 05:34:11', '2018-05-07 05:34:11', '2019-05-07 11:34:11'),
('656712a31b32041b48ecc50c51ac5d4d38eb90b28e924be7d30596183d8d4ce95d113ad40c2e9da4', 1, 3, NULL, '[]', 0, '2018-05-07 07:29:46', '2018-05-07 07:29:46', '2019-05-07 13:29:46'),
('831ddf7fff1f1fb608d6d01c11bfcdd1f09cc41c20eaff2b242a5fc38c2115d664bfe996ddb4e144', 1, 3, NULL, '[]', 0, '2018-05-07 08:05:53', '2018-05-07 08:05:53', '2019-05-07 14:05:53'),
('8df422dd337673098b482db09eac148cd37e520f9c80a636ef734adf22caa7b76be5f608d2f1ed5b', 1, 3, NULL, '[]', 0, '2018-05-07 06:51:17', '2018-05-07 06:51:17', '2019-05-07 12:51:17'),
('925309822b251ad0f5bbe3850b42eb9ce6985cefcd3adf6ac8a02ff240f14e723eb2aa384efbdde3', 1, 3, NULL, '[]', 0, '2018-05-07 07:31:09', '2018-05-07 07:31:09', '2019-05-07 13:31:09'),
('93ee70d0ae53713621a6f1bfe5c02cdbee87b26a429ba9a001eefcc6efef04b0db20d17a8349ea6d', 1, 3, NULL, '[]', 0, '2018-05-07 07:34:24', '2018-05-07 07:34:24', '2019-05-07 13:34:24'),
('967329c79fe928c8fd1494f38648c1b358c6a9a22b6c26e3be9c05943b14d57f150b8aea8a09c8e3', 1, 3, NULL, '[]', 0, '2018-05-07 06:51:53', '2018-05-07 06:51:53', '2019-05-07 12:51:53'),
('b1f01262be120e64617c594f113261749dc62101bbe4cdbd15a3f9b6c1afbd69abf13922cecfe4d6', 1, 3, NULL, '[]', 0, '2018-05-07 08:07:14', '2018-05-07 08:07:14', '2019-05-07 14:07:14'),
('b21a5ca736eab79571c578ba5229d4b00d3759941d3b705d1f73340da3e011aeb4cbec41e55c579e', 1, 3, NULL, '[]', 0, '2018-05-07 06:52:05', '2018-05-07 06:52:05', '2019-05-07 12:52:05'),
('be2566e1551cc32c8e0055ffc7539e340bbfb171a609ed944f62a00206bf3d2da3731a2e4631d115', 1, 3, NULL, '[]', 0, '2018-05-07 07:33:52', '2018-05-07 07:33:52', '2019-05-07 13:33:52'),
('ccfe143b161b4865437e26e744dfc669f1badb2b4d57469e135c27d574ec22d13a1423615102f7da', 1, 3, NULL, '[]', 0, '2018-05-07 07:39:32', '2018-05-07 07:39:32', '2019-05-07 13:39:32'),
('dc59af4617601db4face3bb9616fa0e6e65590f27429c2b80b7c77ba693af5fb5a1aae42acc49c4c', 1, 3, NULL, '[]', 0, '2018-05-07 08:09:47', '2018-05-07 08:09:47', '2019-05-07 14:09:47'),
('de977b76930c89b1d97b4c0c4bd3f06393afd9da327ed3f26e777a188681db42bb526a51f80f6301', 1, 3, NULL, '[]', 0, '2018-05-07 07:23:29', '2018-05-07 07:23:29', '2019-05-07 13:23:29'),
('e0ac589d4b9e4eb8758f55e33dd14ce05506d501835b224e65c3f78d2fdf4676746a6ab6c9b41fa0', 1, 3, NULL, '[]', 0, '2018-05-07 07:37:42', '2018-05-07 07:37:42', '2019-05-07 13:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_auth_codes`
--

INSERT INTO `oauth_auth_codes` (`id`, `user_id`, `client_id`, `scopes`, `revoked`, `expires_at`) VALUES
('03b2d59297a8afa64b6e55afc80b2ae15c5aca69420fba0ee1dab76603f21bab76538635b1b22d1d', 1, 3, '[]', 1, '2018-05-07 13:49:32'),
('06816118aa55f86ccca8e9067a0462110017923a5212cf40f6f05463936a744b67fe39c45f4e0f4c', 1, 3, '[]', 1, '2018-05-07 13:44:38'),
('08ad2da0d3878d642bc4cb4edd0c1588985c4bec43e716921c87d6e865b26b5e62b7d0f02f906dd5', 1, 3, '[]', 1, '2018-05-07 13:48:03'),
('0aa3e0297481eeb98c6de82f692747ba97b91d085acbd833eb0d7777450010e6049d4f017d7e6ab2', 1, 3, '[]', 1, '2018-05-07 13:01:52'),
('0fa661cf4a5195237a87028350217099eee5b3124578b60b9d73c2b4502556c9e8ffc316ebcb8dd8', 1, 3, '[]', 1, '2018-05-07 13:44:23'),
('3428ebb7b1707c27bc2c91efd882de55f7f20a468375e13d2f9ca61681ea5661cf7a4d5cc6d43596', 1, 3, '[]', 1, '2018-05-07 13:39:45'),
('3a69acb8def953e50141695538e37ab96c5c1e71acc5e8076caf8f58f358a3996bb84fef26f4b0ca', 1, 3, '[]', 1, '2018-05-07 13:46:37'),
('3af92bce8a4d8c024265ae85e734cf6eab62b576fe03e5fd3fb595f48804890567343a2d8fefac9d', 1, 3, '[]', 1, '2018-05-07 11:53:05'),
('3f14fb3bcda65d72b856c099957e674efd51f23864601a8ecb7b3037d1b1f0b9ac251b224e9152ee', 1, 3, '[]', 1, '2018-05-07 14:19:46'),
('4920d25307dc573d96d228f6800b449a95c4e8b5a1bbca7a24ca10c83d1cd3db5155a9b5564a8073', 1, 3, '[]', 1, '2018-05-07 13:02:04'),
('4a118d874c79b72ec0c3dfbb092806e6506703b37952241c0bd8f461bd534302520f5d2a900799ae', 1, 3, '[]', 1, '2018-05-07 13:55:10'),
('516adc5c54d7675479840cdd928b799e9bc9899f5e31f87c548150943bd42344bff188aa44cd8dc0', 1, 3, '[]', 1, '2018-05-07 14:18:36'),
('516cc2eb925d3c0f443a2876f5e1630c2de81d75089852ffbbe6df7013dd7061ffa9c434e76e865c', 1, 3, '[]', 1, '2018-05-07 11:44:10'),
('522d1ee1f64b68d6b6ab7ef3ddaa1f6f8d89f8ea857b0a5f2b7502d4554fab6d75b714e598ce8534', 1, 3, '[]', 1, '2018-05-07 13:01:16'),
('52c0f50a68030cd25af0269abf063ebcce429b2ec584e3d373a8698c8213b13f5a6a3bcd32be7e0a', 1, 3, '[]', 0, '2018-05-07 11:34:38'),
('569f12506a5d34423599a643938db4e82e3bd1aeff070ee4b1a4e19fb82106486c1b0dd2bb730e1e', 1, 3, '[]', 1, '2018-05-07 13:54:10'),
('6328d467c1213c0eafc21f8fd79aa1fcc7876b1e141fd0c8490cee8315d9ab84619451471797a59b', 1, 3, '[]', 1, '2018-05-07 13:33:28'),
('6ca9897118eb570571df1a4da73a34332a63068bb447a7b26470af525e805356e195242f8971a697', 1, 3, '[]', 1, '2018-05-07 13:01:24'),
('6f9984680aa20dbd508dd6fc13094890067df168f3e0596fff3b433c65c586a110edc33d7caac1d4', 1, 3, '[]', 1, '2018-05-07 13:38:14'),
('910599a893ec7a036047d02092a34f480064f4593d5c842efd2e84a8167fe3a088444f1aa4891b50', 1, 3, '[]', 1, '2018-05-07 13:48:41'),
('92aaa2ce700edfb7f7d37d0f4934848364bcaccdfb268fa57ff7b55f348bcca7dfc4362536ea5b7a', 1, 3, '[]', 0, '2018-05-07 11:29:11'),
('937de3f8c9471d0c88641f885a63e1f6a833435f1b9e035064b9a0909cc25919bc6c316ff770dd74', 1, 3, '[]', 1, '2018-05-07 13:40:12'),
('ac7673937305d6f0356bb898333afb8d7ff740560197c0fc3e29c8a437ac7824c89c5f901900bb0e', 1, 3, '[]', 1, '2018-05-07 13:45:25'),
('b00182f561081b279c3b8f0c0f70d636b3e206ee6451acf5e1359c7094404b1a0e6e0ce67d7c5a9a', 1, 3, '[]', 1, '2018-05-07 13:41:08'),
('b02fc17d9db92b58c4fac6f4998492c66ac81373caa42583cc753ea670a33583a07379d8136701d5', 1, 3, '[]', 1, '2018-05-07 13:43:51'),
('bdb586a54b7345a0b4f0e014002366c49909fcc9f34a43cf6fc1525cc9aa3b40b041a8b20588bf94', 1, 3, '[]', 1, '2018-05-07 13:47:42'),
('d9fa55aca8d6da5938cc8536004710b520091b72bd5c7c6cf3cfc44736a1535ec69483bc2b2137d6', 1, 3, '[]', 1, '2018-05-07 13:40:47'),
('dad3f941d395988168e0cc1eda630e04abba9de8f662b839039c6d8753adf94c97d274b662539518', 1, 3, '[]', 1, '2018-05-07 14:15:52'),
('ee5090e436bbc62116f4a95bb8e6797b57da24bea9c76f790f03095674926113ad87d01bceda1c9c', 1, 3, '[]', 1, '2018-05-07 13:01:07'),
('f36eec65c8077dcddccf193fae3c62e95e652f1a1d7a7a2078eb7178066e58f68baf18eec91869f3', 1, 3, '[]', 1, '2018-05-07 11:51:40'),
('fcfc8530a4f76a86d5f49a509301c93e712c111c3e9e812eac51de147ba14d92410fd24ea4363cff', 1, 3, '[]', 1, '2018-05-07 14:17:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'RuiWsjjx4d1lDXTqE4yQz2FzhXiEQI7tMpboYbtc', 'http://localhost', 1, 0, 0, '2018-05-05 16:27:18', '2018-05-05 16:27:18'),
(2, NULL, 'Laravel Password Grant Client', 'OeOcT5KxtJa3rEH5Iha8i7BntLFj1xWqon6pKKan', 'http://localhost', 0, 1, 0, '2018-05-05 16:27:18', '2018-05-05 16:27:18'),
(3, 1, 'Clien Application', 'KBfBSkBUo8PyAlWnNJqDfyokz2MXy31QSXtjyC8r', 'http://client-app.dev/callback', 0, 0, 0, '2018-05-06 13:31:13', '2018-05-06 13:31:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-05-05 16:27:18', '2018-05-05 16:27:18');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('0709267ba4a77baf6e7cef9a2306b5e4725136dd84ca4bfc111ba9b5db5681a5d9569b440a2cb3fe', '23e7b375fbff128214e471237028de5c9140dd31f84c61802855d4b6c4ed908640f1bb761af7b572', 0, '2019-05-07 12:51:08'),
('1f80ce60ee441b4dbeafc7c7d9c4dc1baad44e08f1eb0c0c6b03dc53508295ae28f9667b9d5e9bc7', '03e08927d945a415f71f439ebc0b01efe88c0c537d6fc98e0541cfc468dab48e4584fdada6b9fdf8', 0, '2019-05-07 13:30:13'),
('29a738954d43d7ccdef293c46c8b1a1ce5613cff815e42c50b06b8180e782bae968180ce564e2af5', '158704499c47a5f7f4cecfe8e833b1765c989a1ed4978d8aaf7aaa287cf046f7ca690d5b3c7f6e51', 0, '2019-05-07 13:45:11'),
('2d813e010ed2b6ac1490dd0584a06ffe4bdd897aea2458216d612600e99bb69f19270c173f526bec', '8df422dd337673098b482db09eac148cd37e520f9c80a636ef734adf22caa7b76be5f608d2f1ed5b', 0, '2019-05-07 12:51:17'),
('33f36bc210c1714377d16e031a6edf710b0a2f9bef85c2549930f463d4c04ba5ee9b7376f977494d', '3c888b9ec833ef8d2ec2b327594df264ac3ec92dc3173fe9bbac3edd0fac0ae134b6718bb146390c', 0, '2019-05-07 13:30:47'),
('5149d48b5624d9dd93086f367c5ed977353f64abbaeac489ccd70244b609d52f02a6068158105c8c', 'dc59af4617601db4face3bb9616fa0e6e65590f27429c2b80b7c77ba693af5fb5a1aae42acc49c4c', 0, '2019-05-07 14:09:47'),
('550edffc66fe0775b024101da720bc54fecd3a717a803a61ac9715cdfb666a09efdc7d981b25123d', '3d5a12846cab434ea96964eb2c66b1c086eb4fa119ee4619f813a91e1d584a96f816531b918a9ad3', 0, '2019-05-07 13:44:11'),
('7200114ba62b6ff86c211d5356a65739de66c6a0c11b5f75ec9b1bdc46e6db4fc113eb5211e256a8', '29ba8c379ddf5adc867217d3931dfef4b75c4228e19a2caa18cb1ccdf4f774957f1cfee511a84cc8', 0, '2019-05-07 11:41:40'),
('73acc0163822462e43d62bfd2da250627d12ae234e30de21a1ae0ebaa036022bab31c03d20891785', '192b241470314dd80c309cbbfe89d324e484b202edd57982b3cae454d2e1d1c1e8e27fb189ec129a', 0, '2019-05-07 13:38:04'),
('789827ba9a7aeef83b9567a94bc71a43ff01b993f4ff63de7db23fb8e9acd9056e6b3d1085d5d57c', 'de977b76930c89b1d97b4c0c4bd3f06393afd9da327ed3f26e777a188681db42bb526a51f80f6301', 0, '2019-05-07 13:23:29'),
('7b1cde1c9b2023d11b1e41b42bf1f27501ea489611a19a68cd17a2b5522b0e12dbd0ed48ad0eaeba', '656712a31b32041b48ecc50c51ac5d4d38eb90b28e924be7d30596183d8d4ce95d113ad40c2e9da4', 0, '2019-05-07 13:29:46'),
('7be8c3d50b21ab9ce8a229101d744045ffe8d770f4773a3218d0fa39162e9b1a71bb4d1fe7a7576e', '4e5cfd9896cea90ac8e009b44114311fc766b95e92c816d720d619c221bcc08d6e618e9dbc0644fc', 0, '2019-05-07 13:38:41'),
('7d597cc4d488b151f57133e3aa5ac03e3fe6f24aeaf832672256dbe71bed10608ec364d03daf1279', '051f1cfe4c9e62acfe23caabc5fb301125c6a73a604c5e43a603078fdd126d591413e13a20ea9045', 0, '2019-05-07 13:28:15'),
('7ec0a3a48494f5d1a2f7b317feded90332019e06683b4dbdb092b1fc1dc7c5129ddddc29b2a7fd29', '3c4cdbfbe496eb027cf437e6c3922f4715ed288f10816f413506d4436abf60edb253efc546498bc2', 0, '2019-05-07 12:51:24'),
('80ec4eafa1252def3847f68e5272c3952f6f675029341f9e64d03d2d18dd70198416d20c588e0ab1', '06e81f3e4cb247bd1c84fb24d38fa984595bcfc5a2219e88511a3aec770d4ac0823ceb0e1a37c73e', 0, '2019-05-07 11:43:06'),
('8efe7c0a8ee5003b3d0cad2f0ff89274616db18c7a1007eb50022e4c731a6ea0d8c882488e9b17ac', '967329c79fe928c8fd1494f38648c1b358c6a9a22b6c26e3be9c05943b14d57f150b8aea8a09c8e3', 0, '2019-05-07 12:51:53'),
('92631b6b9190c00f3612e04b25b5ab3d4f7bc6f8a06a6b8628feedaf7343798a804baccaeffa72e0', 'be2566e1551cc32c8e0055ffc7539e340bbfb171a609ed944f62a00206bf3d2da3731a2e4631d115', 0, '2019-05-07 13:33:52'),
('a484c148c6275635afc04b6b360f93057b71a41e1713d8808018725f438605fff9d5831b6abddce2', '831ddf7fff1f1fb608d6d01c11bfcdd1f09cc41c20eaff2b242a5fc38c2115d664bfe996ddb4e144', 0, '2019-05-07 14:05:53'),
('a6ab1fccfa0ee12ddc05d8acd9d18ed01ca94fa4270cbf3590b1dcfc44919757d6a2e57266c6dd79', 'b1f01262be120e64617c594f113261749dc62101bbe4cdbd15a3f9b6c1afbd69abf13922cecfe4d6', 0, '2019-05-07 14:07:14'),
('a9211f4bb82739e6c6c11d5f945cb07fe297c2d4163eea6c2d867f0fcdc46116b3d53a531617029c', '530f72b7d4849effdfedab38a8db8a0730f7932565c3036218949038acfcc9da5fca4d2e8c3ac9a6', 0, '2019-05-07 11:34:11'),
('aa6d180696afb5edac2ccc2b9a41fb28685bf3bffc8557114f8ead64cdbc3e06626865b08ad3c8f6', '305bf6053fa42bf6119b4cbd8192c07cbfb0e2361fc6b3f499de592530df0ab17139399cac454126', 0, '2019-05-07 13:36:37'),
('bc54841dcba24548d758fa2f9a9f3b67c7a61648386890f7fdcc12bbb495fafa95e0976c18988461', 'ccfe143b161b4865437e26e744dfc669f1badb2b4d57469e135c27d574ec22d13a1423615102f7da', 0, '2019-05-07 13:39:32'),
('bced2b44f5ff206b6abcc210c8bc0b14e1d0c5a1e865d7517ea5eb222c0d0dde04c2e421803fbe14', 'b21a5ca736eab79571c578ba5229d4b00d3759941d3b705d1f73340da3e011aeb4cbec41e55c579e', 0, '2019-05-07 12:52:05'),
('be5f99826a5b9c8a0ac0b0a1e4b430b8a5baf76d63487429cf4d50d434dfdeea69de82922c20cd90', '2248122d721be2a66344d26c5f8d0899a25a2dfac9603cca075f13ce563a99574464924e2b4cf8a2', 0, '2019-05-07 13:35:26'),
('c80b447aef8ae365a01ad162cb4eea21559b7f31a0850174aa359d203fbcad2a2e29a72c29ec30fa', '3b1e3991303b685d5b7a0f044ac0961c5604ecdffd5741b99b157de99218fa6530eedb4368f62589', 0, '2019-05-07 14:08:37'),
('e3e21ee670876a1cf85ae5550cc7faaaf7f459ee71b2280e51a24a43ee64461ab84ceba9087ddcc9', '93ee70d0ae53713621a6f1bfe5c02cdbee87b26a429ba9a001eefcc6efef04b0db20d17a8349ea6d', 0, '2019-05-07 13:34:24'),
('f02fbb95ce86b0e4d940bc5e365faff2a2a18a0630d9dfcc38f35ac1a8db6011be918da26e7ab674', 'e0ac589d4b9e4eb8758f55e33dd14ce05506d501835b224e65c3f78d2fdf4676746a6ab6c9b41fa0', 0, '2019-05-07 13:37:42'),
('f4c11ffd079670f9d15b3d818bf3dc9f5387960a6cf1af122af0551912371185cc93fb0fff1f96bb', '925309822b251ad0f5bbe3850b42eb9ce6985cefcd3adf6ac8a02ff240f14e723eb2aa384efbdde3', 0, '2019-05-07 13:31:09'),
('f820b399f88d5846446e12677d06fc231ed111ea1033360bc341d2b7a52d518a6f68b08351b79800', '377ed02a1096879236e848c540f0c735076da7e73a0c7997b2ce962748c20f5c80989ae6f7ce6d50', 0, '2019-05-07 13:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mizanur Rahman', 'shimul@gmail.com', '$2y$10$.f4701pP2xtP.PwMIBN4YuP2Ql1WYKyr6g8McUc3Wr4NMlqb6Lb2q', 'SBmr4yba1u47AgUIsGczLdSn4XcVxdgDlJNLYI1ojncnsHjyHnzt1AIHvzvI', '2018-05-06 08:32:24', '2018-05-06 08:32:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
